'use strict';

var generators = require("yeoman-generator");

/**
 *  You can creat a yeoman generator by creating a methods object, and pass it to:
 *  module.exports = generators.Base.extend(methods);
 *  OR, you can first extend the base generator, save reference to resulting generator object in a variable
 *  and then append functions to the prototype.
 */

var ngSlawojGenerator = generators.Base.extend({
    constructor: function () {

        this.processedFiles = [];
        this.answers = [];

        generators.Base.apply(this, arguments);
        //the below makes it available elsewhere in generator under this.sass

        //arguments are passed as such: yo jstack sass
        //this.argument("sass", {
        //    required: false,
        //    desc: "Use classic SASS syntax instead of SCSS"
        //});

        //options are passed as such: yo jstack --sass
        this.option("noInstall", {
            required: false,
            desc: "Do not install npm and bower dependencies"
        });
    }
});

require('./src/001_init')(ngSlawojGenerator);
require('./src/002_prompting')(ngSlawojGenerator);
require('./src/003_configuring')(ngSlawojGenerator);
require('./src/004_install')(ngSlawojGenerator);
require('./src/005_writing')(ngSlawojGenerator);

module.exports = ngSlawojGenerator;