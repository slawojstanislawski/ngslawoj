(function() {
  'use strict';

  angular
    .module('<%- answers.appName %>', [
      <%- modulesDependencies %>,
      '<%- answers.appName %>.core',
      '<%- answers.appName %>.layout',
      '<%- answers.appName %>.dashboard',
      '<%- answers.appName %>.about'
  ]);

})();
