(function() {
    'use strict';

    angular.module('<%- answers.appName %>.dashboard', [
        '<%- answers.appName %>.core',
      ]);
})();
