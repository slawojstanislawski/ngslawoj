(function() {
  'use strict';

  angular
    .module('<%- answers.appName %>')
    .config(config);

  /** @ngInject */
  function config($logProvider) {
    // Enable log
    $logProvider.debugEnabled(true);

  }

})();
