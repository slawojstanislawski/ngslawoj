(function() {
    'use strict';

    angular.module('<%- answers.appName %>.layout', ['<%- answers.appName %>.core']);
})();
