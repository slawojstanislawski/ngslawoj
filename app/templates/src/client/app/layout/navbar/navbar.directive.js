(function() {
  'use strict';

  angular
    .module('<%- answers.appName %>.layout')
    .directive('acmeNavbar', acmeNavbar);

  /** @ngInject */
  function acmeNavbar() {
    var directive = {
      restrict: 'E',
      templateUrl: 'app/layout/navbar/navbar.html',
      scope: {
          creationDate: '='
      },
      controller: NavbarController,
      controllerAs: 'vm',
      bindToController: true
    };

    return directive;

    /** @ngInject */
    function NavbarController(moment, routerHelper) {
      var vm = this;
      var states = routerHelper.getStates();

      activate();

      function activate() { getNavRoutes(); }

      function getNavRoutes() {
        vm.navRoutes = states.filter(function(r) {
          return r.settings && r.settings.nav;
        }).sort(function(r1, r2) {
          return r1.settings.nav - r2.settings.nav;
        });
      }


      // "vm.creation" is avaible by directive option "bindToController: true"
      vm.relativeDate = moment(vm.creationDate).fromNow();
    }
  }

})();
