/* jshint -W117, -W030 */
describe('core', function() {
    describe('state', function() {
        var views = {
            four0four: 'app/core/404.html'
        };

        beforeEach(function() {
            module('<%- answers.appName %>.core', bard.fakeToastr);
            bard.inject('$location', '$rootScope', '$templateCache', '$state');
            /**
             * Having read that https://github.com/angular-ui/ui-router/issues/212#issuecomment-69974072
             * it's clear you need to put templates into templateCache so there were no unexpected GET requests.
             * and that's what this is all setup to do by default.
             * below are some hacks I tried before realizing the templateCache was set in module 'app' instead of 'app.core'.
             * It now works.
             **/
            //module('app.core', function($urlRouterProvider) { //this doesn't work, contrary to what's here:
            //    $urlRouterProvider.deferIntercept();
            //});
            //$httpBackend.whenGET(/app*/).respond(200, ''); //ensure the calls to all and any of the templates starting with "app" (all the views), are expected.
        });

        it('should map /404 route to 404 View template', function() {
            expect($state.get('404').templateUrl).to.equal(views.four0four);
        });

        it('of dashboard should work with $state.go', function() {
            $state.go('404');
            $rootScope.$apply();
            expect($state.is('404'));
        });

        it('should route /invalid to the otherwise (404) route', function() {
            $location.path('/invalid');
            $rootScope.$apply();
            expect($state.current.templateUrl).to.equal(views.four0four);
        });
    });
});
