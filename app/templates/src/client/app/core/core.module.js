(function () {
    'use strict';

    angular
        .module('<%- answers.appName %>.core', [
            //'ngAnimate', 'ngSanitize',
            'blocks.exception', 'blocks.logger', 'blocks.router',
            'ui.router',
            //'ngplus'
        ]);
})();
