/* global malarkey:false, toastr:false, moment:false */
(function () {
    'use strict';

    angular
        .module('<%- answers.appName %>')
        .constant('moment', moment);
    //.constant('malarkey', malarkey)
    //.constant('toastr', toastr)
})();
