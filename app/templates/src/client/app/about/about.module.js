(function() {
    'use strict';

    angular.module('<%- answers.appName %>.about', [
        'app.core'
    ]);
})();
