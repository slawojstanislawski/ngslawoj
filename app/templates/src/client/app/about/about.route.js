(function() {
    'use strict';

    angular
        .module('<%- answers.appName %>.about')
        .run(appRun);

    appRun.$inject = ['routerHelper'];
    /* @ngInject */
    function appRun(routerHelper) {
        routerHelper.configureStates(getStates());
    }

    function getStates() {
        return [
            {
                state: 'about',
                config: {
                    url: '/about',
                    templateUrl: 'app/about/about.html',
                    //controller: 'NoControllerNeededForThisStaticPage',
                    controllerAs: 'vm',
                    title: 'About',
                    settings: {
                        nav: 2,
                        content: '<i class="fa fa-info"></i> About'
                    }
                }
            }
        ];
    }
})();
