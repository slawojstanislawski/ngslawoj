module.exports = function() {
    var client = './src/client/';
    var clientApp = client + 'app/';
    var report = './report/';
    var root = './';
    var server = './src/server/';
    var specRunnerFile = 'specs.html';
    var temp = './.tmp/';
    var wiredep = require('wiredep');
    var bowerFiles = wiredep({devDependencies: true})['js'];
    var nodeModules = 'node_modules';

    var config = {
        /**
         * Files paths
         */
        alljs: [
            './src/**/*.js',
            './*.js'
        ],
        build: './build/',
        client: client,
        clientApp: clientApp,
        css: temp + 'styles.css',
        fonts: [
            './bower_components/font-awesome/fonts/**/*.*',
            './bower_components/bootstrap-sass-official/assets/fonts/**/*.*'
        ],        html: clientApp + '**/*.html',
        htmltemplates: clientApp + '**/*.html',
        images: client + 'img/**/*.*',
        index: client + 'index.html',
        js: [
            clientApp + '**/*.module.js',
            clientApp + '**/*.js',
            '!' + clientApp + '**/*.spec.js'
        ],
        report: report,
        root: root,
        sass: client + 'scss/styles.scss',
        allSassFiles: [
            client +  'app/**/*.scss',
            //'!' + client +  'index.scss'
            client + 'scss/styles.scss'
        ],
        sassTemp: temp + "styles.scss",
        server: server,
        temp: temp,

        /**
         * optimized files
         */
        optimized: {
            app: 'app.js',
            lib: 'lib.js'
        },

        /**
         * template cache
         */
        templateCache: {
            file: 'templates.js',
            options: {
                module: '<%= answers.appName %>.core',
                standAlone: false,
                root: 'app/'
            }
        },

        /**
         * browser sync
         */
        browserReloadDelay: 1000,

        /**
         * Bower and NPM locations
         */
        bower: {
            json: require('./bower.json'),
            directory: './bower_components/',
            //filesToBeExcluded: [/jquery/, /bootstrap.js$/, /bootstrap-sass-official\/.*\.js/, /bootstrap\.css/],
            ignorePath: '../../'
        },
        packages : [
            './package.json',
            './bower.json'
        ],

        /**
         * specs.html, our HTML spec runner
         */
        specRunner: client + specRunnerFile,
        specRunnerFile: specRunnerFile,
        testlibraries: [
            nodeModules + '/mocha/mocha.js',
            nodeModules + '/chai/chai.js',
            nodeModules + '/sinon-chai/lib/sinon-chai.js'
        ],
        specs: [clientApp + '**/*.spec.js'],

        /**
         * Karma and testing settings
         */
        specHelpers: [client + 'test-helpers/*.js'],
        serverIntegrationSpecs: [client + 'tests/server-integration/**/*.spec.js'],

        /**
         * Node settings
         */
        defaultPort: 7203,
        nodeServer: './src/server/app.js'

    };

    config.getWiredepDefaultOptions = function() {
        var options = {
            bowerJson: config.bower.json,
            exclude: config.bower.filesToBeExcluded,
            directory: config.bower.directory,
            ignorePath: config.bower.ignorePath
        };
        return options;
    };

    config.karma = getKarmaOptions();

    return config;

    ////////////////

    function getKarmaOptions() {
        var options = {
            files: [].concat(
                bowerFiles,
                config.specHelpers,
                client + '**/*.module.js', //load module js first...
                client + '**/*.js', //and then all the other js files
                temp + config.templateCache.file,//angular template cache
                config.serverIntegrationSpecs
            ),
            exclude: [],
            coverage: {
                dir: report + 'coverage',
                reporters: [ //reporters will tell you about the coverage
                    {type: 'html', subdir: 'report-html'},
                    {type: 'lcov', subdir: 'report-lcov'}, //some jenkins thing, not important
                    {type: 'text-summary'}
                ]
            },
            preprocessors: {} //defined right below
        };
        options.preprocessors[clientApp + '**/!(*.spec)+(.js)'] = ['coverage']; //get all js files except .spec files. So no coverage for test files ;)
        return options;
    }
};
