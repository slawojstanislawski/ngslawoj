module.exports = function (ngSlawojGenerator) {
    ngSlawojGenerator.prototype.install = function install() {
        if(!this.options.noInstall) {
            this.bowerInstall();
            this.npmInstall();
        }
    }
};