var prompts = require("../prompts.json");
var _ = require('lodash');


module.exports = function (ngSlawojGenerator) {

    ngSlawojGenerator.prototype.prompting = function prompting() {
        var done = this.async();
        this.prompt(this._getPrompts(), function (answers) {
            this._saveAnswers(answers, done)
        }.bind(this));
    };

    ngSlawojGenerator.prototype._getPrompts = function _getPrompts() {
        this.appName = this.appname;
        return [
            {
                name: "appName",
                message: "What is the name of your project?",
                default: this.appName
            }
        ].concat(prompts);
    };

    ngSlawojGenerator.prototype._saveAnswers = function _saveAnswers(answers, done) {

        //set the answers to be available in the generator

        this.answers = answers;

        //build a list of required modules thanks to the provided answers

        var ngModules = this.answers.angularModules.map(function (module) {
            return module.module;
        });

        //to those modules add the modules from other questions like a chosen resource module, router module, ui module etc.
        ngModules = ngModules.concat([
            this.answers.resource.module,
            this.answers.router.module,
            //this.answers.ui.module,
            this.answers.bootstrapComponents.module,
            //this.answers.foundationComponents.module
        ]);

        //make a string of space-separated module names and put them in the later-used this.modulesDependencies property.
        //it will be used in template for index.module.js file, to inject app-wide dependencies.
        var modulesDependencies = ngModules
            .filter(_.isString)//answers.foundationComponents or answers.bootstrapComponents - one of them will not be a string but a false boolean :)
            .map(function (dependency) {
                return '\'' + dependency + '\'';
            })
            .join(', ');
        this.modulesDependencies = modulesDependencies;

        // Simplify the model to simplify access to angular modules from the templates
        // change the keys from the question-received list of modules to actual module names.
        // prepare angularModulesObject for use in bower.json template:)
        this.angularModulesObject = {};
        this.answers.angularModules.forEach(function (module) {
            this[module.key] = module.module;
        }, this.angularModulesObject);


        /**
         *  I decided to only use ui router from now on :) leave only this one line of this.routerHtml
         */
        this.routerHtml = '<div ui-view></div>';

        done();
    };

};