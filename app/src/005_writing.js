var mkdirp = require("mkdirp");
var files = require("../files.json");


module.exports = function(ngSlawojGenerator) {

    ngSlawojGenerator.prototype.writing = function writing() {
        this._createProjectFileSystem();
    };

    ngSlawojGenerator.prototype._resolvePaths = function _resolvePaths(template) {
        return function (file) {
            var src = file, dest = file;

            return {
                src: src,
                dest: dest,
                template: template
            };
        };
    };

    ngSlawojGenerator.prototype._createProjectFileSystem = function _createProjectFileSystem() {
        //var sourceRoot = this.sourceRoot();
        var destRoot = this.destinationRoot();
        var destClient = destRoot + "/src/client/";

        //USING ARGUMENTS AND OPTIONS IN THE GENERATOR, JUST IN CASE.
        //when using arguments:
        //var sassFileExtension = (this.sass) ? ".sass" : ".scss";
        //when using options
        //var sassFileExtension = (this.options.sass) ? ".sass" : ".scss";

        var templateContext = {
            answers: this.answers,
            modulesDependencies: this.modulesDependencies,
            routerHtml: this.routerHtml,
            angularModulesObject: this.angularModulesObject
        };
        mkdirp(destClient + "img");

        this.processedFiles = this.processedFiles
            .concat(files.staticFiles.map(this._resolvePaths(false)))
            .concat(files.templates.map(this._resolvePaths(true)));

        this.processedFiles.forEach(function (file) {
            try {
                if (file.template) { //AAAAHAAAA, SO that's what the template boolean variable was for :D:D:D
                    this.fs.copyTpl(this.templatePath(file.src), this.destinationPath(file.dest), templateContext);
                } else {
                    this.fs.copy(this.templatePath(file.src), this.destinationPath(file.dest));
                }
            } catch (error) {
                console.error('Template processing error on file', file.src);
                throw error;
            }
        }, this);

    };

};