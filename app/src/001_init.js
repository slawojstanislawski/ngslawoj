var chalk = require("chalk");
var yosay = require("yosay");

module.exports = function(ngSlawojGenerator) {
    ngSlawojGenerator.prototype.initializing = function initializing() {
        var message = chalk.yellow.bold("Welcome to ngSlawoj ") + chalk.yellow("A solid JS stack to develop with");
        this.log(yosay(message, {maxLength: 20}));
    }
};